$(document).ready(function() {
    var upload_path = $('.upload-path');
    $('.upload').on('change', function() {
        if(this.files != undefined){
            upload_path.html(this.files[0].name);
        }
        else{
            upload_path.html('Прикреплено');
        }
    });
    $('.toggle-menu').on('click', function(e){
        e.preventDefault();
        var menu = $('.menu');
        if(menu.hasClass('show')){
            menu.removeClass('show');
        }
        else{
            menu.addClass('show');
        }
    });
    $('.menu-login-link').on('click', function(e){
        e.preventDefault();
        var menu = $('.menu-login-block');
        if(menu.hasClass('show')){
            menu.removeClass('show');
        }
        else{
            menu.addClass('show');
        }
    });
    
    $('.phone-mask').mask('+7(000)000-00-00');
    
    $('.checkbox-input-block').on('click', function(){
        $(this).find('input[type="checkbox"]').click();
        if($(this).find('input[type="checkbox"]').is(':checked')){
            $(this).addClass('checkbox-on');
        }
        else{
            $(this).removeClass('checkbox-on');
        }
    });
    
    $('.input-radio-block').on('click', function(){
        var name = $(this).find('input[type="radio"]').attr('name');
        $('input[name="'+name+'"]').closest('.input-radio-block').removeClass('radio-on');
        $(this).find('input[type="radio"]').prop("checked", true);
        if($(this).find('input[type="radio"]').is(':checked')){
            $(this).addClass('radio-on');
        }
        else{
            $(this).removeClass('radio-on');
        }
    });
    
    if (typeof $.fn.select2 !== 'undefined') {
        $('select').select2({minimumResultsForSearch: -1});
    }
    
    $('.fancybox').fancybox({minWidth: 580});
    $('.j7-fancybox-close').on('click', function(){
        $.fancybox.close();
    });
    
    var datePicker = $( ".datepicker" );

    if(datePicker.length){
        datePicker.datepicker({
            closeText: "Закрыть",
            prevText: "&#x3C;Пред",
            nextText: "След&#x3E;",
            currentText: "Сегодня",
            monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь", "Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
            monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн", "Июл","Авг","Сен","Окт","Ноя","Дек" ],
            dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
            dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
            dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
            weekHeader: "Нед",
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            changeMonth: true,
            changeYear: true,
            //isRTL: false,
            //showMonthAfterYear: false,
            //yearSuffix: "",
            //yearRange: '1930:1998'
        });
    }
    
    $('.faq-item a').on('click', function(e){
        e.preventDefault();
        $('.faq-answer').hide();
        $(this).closest('.faq-item').find('.faq-answer').show();
    })
    
    $('.content-gallery .photo-item .like-link').on('click', function(e){
        e.preventDefault();
        var item = $(this).closest('.photo-item');
        set_like(item);
    })
    
    $('.gallery-zoom-votes').on('click', function(e){
        e.preventDefault();
        var item = $('.photo-item.active');
        set_like(item, true);
    })
    
    $('.content-gallery .photo-item .photo-img').on('click', function(){
        var zoom_modal = $('#gallery-zoom');
        var item = $(this).closest('.photo-item');
        item.addClass('active');
        gallery_modal_insert(zoom_modal, item);
        j7_modal('gallery-zoom');
    })
    
    $('.gallery-slidin-l').on('click', function(){
        var prev = $('.photo-item.active').prev('.photo-item');
        gallery_slidin(prev);
    })
    
    $('.gallery-slidin-r').on('click', function(){
        var next = $('.photo-item.active').next('.photo-item');
        gallery_slidin(next);
    })
    
    $('.prize-nav-prev').on('click', function(e){
        e.preventDefault();
        prize_slidin('prev');
    })
    
    $('.prize-nav-next').on('click', function(e){
        e.preventDefault();
        prize_slidin('next');
    })
    
    function set_like(item, is_modal){
        var votes = parseInt(item.attr('data-votes')) + 1;
        item.attr('data-votes', votes);
        item.find('.like-count').html(votes);
        if(is_modal){
            $('.gallery-zoom-votes .like-count').html(votes);
        }
    }
    
    
    function gallery_slidin(item){
        var active = $('.photo-item.active');
        var zoom_modal = $('#gallery-zoom');
        if(!active.length){
            var active = $('.photo-item').first();
        }
        if(item.length){
            active.removeClass('active');
            item.addClass('active');
            gallery_modal_insert(zoom_modal, item);
        }
    }
    function gallery_modal_insert(zoom_modal, item){
        zoom_modal.find('.gallery-zoom-img img').attr('src', item.data('img'));
        zoom_modal.find('.gallery-zoom-first_name').html(item.data('first_name'));
        zoom_modal.find('.gallery-zoom-last_name').html(item.data('last_name'));
        zoom_modal.find('.gallery-zoom-votes .like-count').html(item.attr('data-votes'));
    }
    function prize_slidin(arr){
        var active = $('.prize-item.active');
        if(arr === 'prev'){
            var item = active.prev('.prize-item');
            if (!item.length) item = $('.prize-item').last();
        }
        if(arr === 'next'){
            var item = active.next('.prize-item');
            if (!item.length) item = $('.prize-item').first();
        }
        active.removeClass('active');
        item.addClass('active');
        
    }
    function j7_modal(id){
        $.fancybox.open({href: '#' + id, wrapCSS: 'modal-'+id});
    }
})
